﻿using System;
using UnityEngine;

/// <summary>
/// Behaviour for anything that can travel from one point to another
/// </summary>
public class TravelBehaviour : FrostAuraBaseGameObject
{
    /// <summary>
    /// The speed at which should be traveled
    /// </summary>
    public float Speed = 15f;
    /// <summary>
    /// The target to which will be traveled
    /// </summary>
    public GameObject Target;
    /// <summary>
    /// The event that occurs when the traveler has reached its destination (this, target)
    /// </summary>
    public event Action<GameObject, GameObject> OnDestinationReached;

    protected override void Update()
    {
        if (Target == null) return;

        // Get the direction from the projectile to the target
        Vector3 direction = Target.transform.position - transform.localPosition;

        // Calculate how far the projectile has gone in this frame
        float distanceThisFrame = Speed * Time.deltaTime;

        if (direction.magnitude <= distanceThisFrame)
        {
            // We have reached the target
            NotifyDestinationReached();
        }
        else
        {
            // Translate the position of the projectile relative towards the target relative to world space
            transform.Translate(direction.normalized * distanceThisFrame, Space.World);

            // Get the rotation of looking towards the target
            Quaternion targetRotation = Quaternion.LookRotation(direction);

            // Apply the look rotation to the projectile
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 5);
        }

        base.Update();
    }

    protected override bool ValidateComponentProperties()
    {
        return true;
    }

    /// <summary>
    /// Notify subscribers that the destination has been reached if the event handler exists
    /// </summary>
    protected void NotifyDestinationReached()
    {
        if (OnDestinationReached != null) OnDestinationReached(gameObject, Target);
    }
}