﻿using UnityEngine;

/// <summary>
/// Particle system behaviour
/// </summary>
[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemBehaviour : FrostAuraBaseGameObject
{
    /// <summary>
    /// Instance of the particle system
    /// </summary>
    private UnityEngine.ParticleSystem ps;

    protected override void Start()
    {
        // Get and assign the instance of the particle system
        ps = GetComponent<UnityEngine.ParticleSystem>();

        base.Start();
    }

    protected override void Update()
    {
        // If the particle system exists
        if (ps)
        {
            // If the particle system is done playing
            if (!ps.IsAlive())
            {
                // Destroy it
                Destroy(gameObject);
            }
        }

        base.Update();
    }

    protected override bool ValidateComponentProperties()
    {
        return true;
    }
}