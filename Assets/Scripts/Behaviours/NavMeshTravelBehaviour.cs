﻿using UnityEngine;

/// <summary>
/// Behaviour for anything that can travel from one point to another on a nav mesh
/// </summary>
public class NavMeshTravelBehaviour : TravelBehaviour
{
    protected override void Update()
    {
        if (Target == null) return;

        // Get the distance between the enemy and it's destination
        float distanceFromTarget = Vector3.Distance(Target.transform.position, transform.position);

        // If the enemy has reached it's destination
        if (distanceFromTarget < 1)
        {
            NotifyDestinationReached();
        }
    }
}