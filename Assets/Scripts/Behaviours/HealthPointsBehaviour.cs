﻿using System;
using UnityEngine;

public class HealthPointsBehaviour : FrostAuraBaseGameObject
{
    /// <summary>
    /// The current amount of health
    /// </summary>
    public int HealthPoints = 100;
    /// <summary>
    /// The event that will occur when the health gets to zero
    /// </summary>
    public event Action<GameObject> OnHealthDepleted;
     
    /// <summary>
    /// Lower the health points by a certain number
    /// </summary>
    /// <param name="points">The number of health to lower by</param>
    public void TakeHealth(int points)
    {
        HealthPoints -= points;

        if (HealthPoints <= 0)
        {
            if (OnHealthDepleted != null) OnHealthDepleted(gameObject);
        }
    }

    protected override bool ValidateComponentProperties()
    {
        return true;
    }
}