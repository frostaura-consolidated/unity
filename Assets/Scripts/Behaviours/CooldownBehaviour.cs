﻿using System;
using UnityEngine;

/// <summary>
/// Behaviour for anything that should implement some type of cooling down
/// </summary>
public class CooldownBehaviour : FrostAuraBaseGameObject
{
    /// <summary>
    /// The interval that a cooldown happens for
    /// </summary>
    public float CooldownTime = 1.0f;
    /// <summary>
    /// The event that fires when the cooldown period has been reached (this)
    /// </summary>
    public event Action<GameObject> OnCooledDown; 
    /// <summary>
    /// The interval of time that has passed since the cooldown started
    /// </summary>
    private float _cooldownTimeLeft = 0f;

    /// <summary>
    /// Reset the cooldown time to zero
    /// </summary>
    public void ResetCooldown()
    {
        _cooldownTimeLeft = 0f;
    }

    protected override void Update()
    {
        if(DebugMode) Debug.Log(string.Format("Cooldown time: {0}/{1}", _cooldownTimeLeft, CooldownTime));

        // Decrease the cooldown time
        _cooldownTimeLeft -= Time.deltaTime;

        // When the cooldown has completed
        if (_cooldownTimeLeft <= 0f)
        {
            if (DebugMode) Debug.Log("Cooldown complete");

            _cooldownTimeLeft = CooldownTime;
            
            // Notify subscribers that the cooldown has completed
            if(OnCooledDown != null) OnCooledDown(gameObject);
        }

        base.Update();
    }

    protected override bool ValidateComponentProperties()
    {
        return true;
    }
}