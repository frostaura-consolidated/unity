﻿using UnityEngine;

/// <summary>
/// Base game object for all FrostAura game objects
/// </summary>
public abstract class FrostAuraBaseGameObject : MonoBehaviour
{
    /// <summary>
    /// Whether or not the properties in this component are all valid
    /// </summary>
    protected bool IsModelStateValid;
    /// <summary>
    /// Whether or not the component is currently in debug mode
    /// </summary>
    public bool DebugMode = false;

    /// <summary>
    /// Method called at the initialization of the object
    /// </summary>
    protected virtual void Start()
    {
        if(DebugMode) Debug.Log(string.Format("Initializing '{0}'", GetType()));

        IsModelStateValid = ValidateComponentProperties();

        if(!IsModelStateValid) Debug.LogError(string.Format("'{0}' is not in a valid state.", GetType()));
    }

    /// <summary>
    /// Method called once per frame
    /// </summary>
    protected virtual void Update()
    { }

    /// <summary>
    /// Methode called at a fixed time as opposed to when it can
    /// </summary>
    protected virtual void FixedUpdate() { }

    /// <summary>
    /// OnDrawGizmos is called every frame. All gizmos rendered within OnDrawGizmos are pickable. OnDrawGizmosSelected is called only if the object the script is attached to is selected.
    /// </summary>
    private void OnDrawGizmos()
    {
        if(DebugMode) DebugUpdate();
    }

    /// <summary>
    /// DebugUpdate is called every frame when the DebugMode flag for an object is true
    /// </summary>
    protected virtual void DebugUpdate()
    {
        // Get the forward direction of the GemwObject
        Vector3 forwardDirection = transform.TransformDirection(Vector3.forward);

        // Cast a ray in the forward direction with a specified length
        Debug.DrawRay(transform.position, forwardDirection * 10, Color.green);
    }

    /// <summary>
    /// Try to get a component type on an object and traverse upwards it's parents a specified max nr of times to get the component of a type
    /// </summary>
    /// <typeparam name="T">The type that we need to get an instance of</typeparam>
    /// <param name="obj">The object on which we want to get the instance of T from</param>
    /// <param name="currentLevel">The current level of traversing</param>
    /// <param name="maxLevels">The maximum leve of traversing allowed</param>
    /// <returns>The instance of T if found, otherwise the default of T (null most likely)</returns>
    protected T TryGetComponentUpwards<T>(GameObject obj, int currentLevel = 1, int maxLevels = 3)
    {
        var componentInstance = obj.InstanceOfOrNull<T>();

        if (componentInstance != null) return componentInstance;

        if(currentLevel == maxLevels) return default(T);

        if (obj.transform.parent == null) return default(T);

        return TryGetComponentUpwards<T>(obj.transform.parent.gameObject, currentLevel + 1);
    }

    /// <summary>
    /// Validate that all the required properties are assigned inside of the component etc
    /// </summary>
    /// <returns>Whether or not the validation was successful</returns>
    protected abstract bool ValidateComponentProperties();
}