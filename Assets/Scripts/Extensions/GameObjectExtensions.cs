﻿using System;
using UnityEngine;

/// <summary>
/// Extensions for all game objects
/// </summary>
public static class GameObjectExtensions
{
    /// <summary>
    /// Try to get the instance of T on a specific game object if it does in fact implement T
    /// </summary>
    /// <typeparam name="T">The type of instance to try and get</typeparam>
    /// <param name="gameObject">The game object on which to try and get the instance of T</param>
    /// <returns>Return the instance of T on the game object or null if non</returns>
    public static T InstanceOfOrNull<T>(this GameObject gameObject)
    {
        try
        {
            return gameObject.GetComponent<T>();
        }
        catch (Exception)
        {
            return default(T);
        }
    }
}