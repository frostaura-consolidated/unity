﻿using UnityEngine;

/// <summary>
/// Extensions for all transform objects
/// </summary>
public static class TransformExtensions
{
    /// <summary>
    /// Transform the current rotation  of the transform object to look at a given other transform object
    /// </summary>
    /// <param name="gameObjectTransform">The transform object to apply the rotation to</param>
    /// <param name="otherObjectTransform">The transform object that should be looked at</param>
    /// <param name="lerpTime">The multiplier for how fast the rotation should occur</param>
    /// /// <param name="drawRay">Whether or not to draw a debug ray to show where the rotation is looking at</param>
    public static void RotateTowardsObject(this Transform gameObjectTransform, Transform otherObjectTransform, float lerpTime = 5.0f, bool drawRay = false)
    {
        // Get the direction from this object's perspective towards the target object
        Vector3 direction = otherObjectTransform.position - gameObjectTransform.position;

        // Generate a look at rotation based on the direction
        gameObjectTransform.rotation = Quaternion.Lerp(gameObjectTransform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * lerpTime);

        // If we should draw a ray, do so
        if(drawRay) Debug.DrawRay(gameObjectTransform.position, direction, Color.red);
    }

    /// <summary>
    /// Reset the rotation of a given transform object
    /// </summary>
    /// <param name="gameObjectTransform">The transform object to apply the rotation to</param>
    /// <param name="lerpTime">The multiplier for how fast the rotation should occur</param>
    /// <param name="defaultRotation">The rotation to reset to</param>
    public static void ResetRotation(this Transform gameObjectTransform, float lerpTime = 5.0f, Vector3 defaultRotation = default(Vector3))
    {
        // If no rotation was specified, default to forward
        if(defaultRotation == default(Vector3)) defaultRotation = Vector3.forward;

        // Update the rotation
        gameObjectTransform.rotation = Quaternion.Lerp(gameObjectTransform.rotation, Quaternion.LookRotation(Vector3.forward), Time.deltaTime * lerpTime);
    }
}