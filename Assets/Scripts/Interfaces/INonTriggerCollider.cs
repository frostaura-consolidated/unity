﻿using UnityEngine;

/// <summary>
/// Interface an object that has a collider that is not a trigger, should implement
/// </summary>
public interface INonTriggerCollider
{
    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun touching another rigidbody/collider.
    /// </summary>
    /// <param name="collisionInfo">The colliding object</param>
    void OnCollisionEnter(Collision collisionInfo);
    /// <summary>
    /// OnCollisionExit is called when this collider/rigidbody has stopped touching another rigidbody/collider.
    /// </summary>
    /// <param name="collisionInfo">The colliding object</param>
    void OnCollisionExit(Collision collisionInfo);
    /// <summary>
    /// OnCollisionStay is called once per frame for every collider/rigidbody that is touching rigidbody/collider.
    /// </summary>
    /// <param name="collisionInfo">The colliding object</param>
    void OnCollisionStay(Collision collisionInfo);
}