﻿using UnityEngine;

/// <summary>
/// Interface an object that has a collider that is trigger, should implement
/// </summary>
public interface ITriggerCollider
{
    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The colliding object</param>
    void OnTriggerEnter(Collider other);
    /// <summary>
    /// OnTriggerExit is called when the Collider other has stopped touching the trigger.
    /// </summary>
    /// <param name="other">The colliding object</param>
    void OnTriggerExit(Collider other);
    /// <summary>
    /// OnTriggerStay is called almost all the frames for every Collider other that is touching the trigger.
    /// </summary>
    /// <param name="other">The colliding object</param>
    void OnTriggerStay(Collider other);
}