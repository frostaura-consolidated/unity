﻿using System;
using UnityEngine;

/// <summary>
/// Projectile entity
/// </summary>
[RequireComponent(typeof (TravelBehaviour))]
public class Projectile : FrostAuraBaseGameObject
{
    /// <summary>
    /// The instance of the travel behaviour
    /// </summary>
    private TravelBehaviour _travelBehaviour;

    protected override void Start()
    {
        _travelBehaviour = GetComponent<TravelBehaviour>();
        _travelBehaviour.OnDestinationReached += (traveler, target) =>
        {
            Destroy(gameObject);
        };

        base.Start();
    }
    
    protected override void Update()
    {
        // If no target exists, destroy the projectile
        if (_travelBehaviour.Target == null)
        {
            Destroy(gameObject);

            return;
        };

        base.Update();
    }

    protected override bool ValidateComponentProperties()
    {
        return true;
    }
}