﻿using UnityEngine;

public class SpawnManager : FrostAuraBaseGameObject{
	public GameObject _enemy;
	public GameObject Target;
	public float cooldown = 10f;
	public float cooldownLeft = 0f;

	protected override bool ValidateComponentProperties()
	{
		return true;
	}

	protected override void Update ()
	{
		cooldownLeft -= Time.deltaTime;

		if (cooldownLeft <= 0) {
			cooldownLeft = cooldown;

			// Spawn Bitch
			var instance = (GameObject)Instantiate(_enemy, new Vector3(transform.position.x, 1, transform.position.z), transform.rotation);
			var instanceOfEnemy = instance.GetComponent<Enemy> ();
			var enemyTraveler = instance.GetComponent<NavMeshTravelBehaviour> ();

			enemyTraveler.Target = Target;

			instanceOfEnemy.ForceStart ();
		}

		base.Update ();
	}
}