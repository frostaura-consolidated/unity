﻿using UnityEngine;

/// <summary>
/// Enemy entity
/// 
/// NOTE: For Enemy entities, the rigitbody has to be kinematic and the rigit body on the nested actual graphic component needs to be the same in order to not have the navmeshagent wander out of sync
/// </summary>
[RequireComponent(typeof(HealthPointsBehaviour), typeof(NavMeshTravelBehaviour))]
[RequireComponent(typeof(NavMeshAgent), typeof(Rigidbody))]
public class Enemy : FrostAuraBaseGameObject
{
    /// <summary>
    /// The instance of the travel behaviour
    /// </summary>
    private NavMeshTravelBehaviour _travelBehaviour;
    /// <summary>
    /// The instance of the enemy navigation agent
    /// </summary>
    private NavMeshAgent _navigationAgent;
    /// <summary>
    /// The instance of the enemy health points behaviour
    /// </summary>
    private HealthPointsBehaviour _healthPointsBehaviour;

    /// <summary>
    /// Method called at the initialization of the object
    /// </summary>
    protected override void Start()
    {
        _travelBehaviour = GetComponent<NavMeshTravelBehaviour>();
        _navigationAgent = GetComponent<NavMeshAgent>();
        _healthPointsBehaviour = GetComponent<HealthPointsBehaviour>();

        _travelBehaviour.OnDestinationReached += TravelBehaviourOnOnDestinationReached;
        _healthPointsBehaviour.OnHealthDepleted += HealthPointsBehaviourOnOnHealthDepleted;

        if (_travelBehaviour.Target != null) _navigationAgent.destination = _travelBehaviour.Target.transform.position;

        base.Start();
    }

    protected override bool ValidateComponentProperties()
    {
        return
            (_navigationAgent != null);
    }


	/// <summary>
	/// Force start
	/// </summary>
	public void ForceStart()
	{
		Start ();
	}

    private void TravelBehaviourOnOnDestinationReached(GameObject traveler, GameObject target)
    {
        Destroy(gameObject);
    }

    private void HealthPointsBehaviourOnOnHealthDepleted(GameObject gameObject)
    {
        Destroy(gameObject);
    }
}