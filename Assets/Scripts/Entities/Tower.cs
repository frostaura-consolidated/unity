﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Tower entity
/// </summary>
[RequireComponent(typeof (SphereCollider), typeof(CooldownBehaviour))]
public class Tower : FrostAuraBaseGameObject, ITriggerCollider
{
    /// <summary>
    /// The amount of hitpoints the tower will subtract upon impact
    /// </summary>
    public int Damage = 10;
    /// <summary>
    /// The projectile that the tower will fire
    /// </summary>
    public GameObject Projectile;
    /// <summary>
    /// The time it takes for the tower to rotate to its desired rotation
    /// </summary>
    public float RotationDuration = 5.0f;
    /// <summary>
    /// The section of the tower that will rotate to lock onto the current target if any (Optional)
    /// </summary>
    public GameObject RotatableSection;
    /// <summary>
    /// The positions from which projectiles will fire
    /// </summary>
    public GameObject[] MuzzlePositions;
    /// <summary>
    /// The particle effect that will spawn when a projectile is fired
    /// </summary>
    public GameObject FireEffect;
    /// <summary>
    /// The scale of the particle effect that's created when a projectile is fired
    /// </summary>
    public Vector3 FireEffectScale = new Vector3(1, 1, 1);
    /// <summary>
    /// The particle effect that will spawn when a projectile impacts a target
    /// </summary>
    public GameObject ImpactEffect;
    /// <summary>
    /// The scale of the particle effect that's created when a projectile impacts a target
    /// </summary>
    public Vector3 ImpactEffectScale = new Vector3(1, 1, 1);
    /// <summary>
    /// The current enemy, if any, that the tower is targetting
    /// </summary>
    private readonly List<Enemy> _currentTargets = new List<Enemy>();
    /// <summary>
    /// The instance of the sphere collider that is responsible for calculating targeting radiuses
    /// </summary>
    private SphereCollider _collider;
    /// <summary>
    /// The instance of the cooldown behaviour for this component
    /// </summary>
    private CooldownBehaviour _cooldownBehaviour;

    protected override void Start()
    {
        if(RotatableSection == null) Debug.LogWarning("No rotatable section was assigned to the tower");

        // Grab the collider
        _collider = GetComponent<SphereCollider>();
        // Grab the cooldown behaviour
        _cooldownBehaviour = GetComponent<CooldownBehaviour>();

        // Tap into when the cooldown has been reached
        _cooldownBehaviour.OnCooledDown += cooldownBehaviour =>
        {
            // If there is no current target, don't proceed
            if (_currentTargets.Count == 0) return;

            // Loop through all the projectile spawn locations on the tower
            foreach (GameObject muzzlePosition in MuzzlePositions)
            {
                SpawnNewProjectile(muzzlePosition);
            }
        };

        base.Start();
    }

    protected override bool ValidateComponentProperties()
    {
        if (Projectile == null)
        {
            Debug.LogWarning("No projectile was assigned to the tower");

            return false;
        }

        if (_cooldownBehaviour == null)
        {
            Debug.LogWarning("No cooldown behaviour was assigned to the tower");

            return false;
        }

        if (MuzzlePositions == null || MuzzlePositions.Length == 0)
        {
            Debug.LogWarning("No muzzle positions were assigned to the tower");
        }

        return true;
    }

    protected override void Update()
    {
        UpdateRotation();

        base.Update();
    }

    protected override void DebugUpdate()
    {
        // Draw a wire sphere for the line of sight for the tower
        if (_collider != null) Gizmos.DrawWireSphere(_collider.center, _collider.radius);

        base.DebugUpdate();
    }

    #region ITriggerCollider Implementations

    public void OnTriggerEnter(Collider other)
    {
        // Get the enemy, if any that is currently colliding
        var enemyInstance = TryGetComponentUpwards<Enemy>(other.gameObject);

        if (enemyInstance != null)
        {
            var enemyHealth = enemyInstance.GetComponent<HealthPointsBehaviour>();

            enemyHealth.OnHealthDepleted += (enemy) => {
                _currentTargets.Remove(enemy.gameObject.InstanceOfOrNull<Enemy>());
            };

            var enemyTraveler = enemyInstance.GetComponent<NavMeshTravelBehaviour>();

            enemyTraveler.OnDestinationReached += (enemy, target) => {
                _currentTargets.Remove(enemy.gameObject.InstanceOfOrNull<Enemy>());
            };

            _currentTargets.Add(enemyInstance);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        // Get the enemy, if any that is currently done colliding
        var enemyInstance = TryGetComponentUpwards<Enemy>(other.gameObject);

        _currentTargets.Remove(enemyInstance);
    }

    public void OnTriggerStay(Collider other)
    { }

    #endregion

    /// <summary>
    /// Update the rotation of the rotatable part of the tower, if any
    /// </summary>
    private void UpdateRotation()
    {
        // If no rotatable section was assigned, don't proceed
        if (RotatableSection == null) return;
        
        // If we have no current target, revert to original rotation and don't proceed
        if (_currentTargets.Count == 0)
        {
            RotatableSection.transform.ResetRotation(RotationDuration);

            return;
        }

        // If we have a target, lock on to it
        RotatableSection.transform.RotateTowardsObject(_currentTargets[0].transform, RotationDuration, DebugMode);
    }

    /// <summary>
    /// Creates a new instance of a projectile from the given muzzle position together with managing particle effects for the projectile
    /// </summary>
    /// <param name="spawnLocation">The position game object from where the projectile should be spawned</param>
    private void SpawnNewProjectile(GameObject spawnLocation)
    {
        // Create a projectile instance
        var projectileInstance = (GameObject)Instantiate(Projectile, spawnLocation.transform.position, RotatableSection.transform.rotation);

        // Create an instance of the projectile prefab
        var projectileScriptInstance = projectileInstance.GetComponent<Projectile>();
        var projectileTravelInstance = projectileInstance.GetComponent<TravelBehaviour>();

        // Set up the target
        projectileTravelInstance.Target = _currentTargets[0].gameObject;

        // Configure debug
        projectileScriptInstance.GetComponent<FrostAuraBaseGameObject>().DebugMode = DebugMode;

        // Instantiate on projectile spawn particle effect if any
        if (FireEffect != null)
        {
            // Create instance of effect
            var fireEffectInstance = (GameObject)Instantiate(FireEffect, spawnLocation.transform.position, RotatableSection.transform.rotation);

            // Configure debug
            fireEffectInstance.GetComponent<FrostAuraBaseGameObject>().DebugMode = DebugMode;

            // Scale the effect instance accordingly
            fireEffectInstance.transform.localScale = FireEffectScale;
        }

        projectileTravelInstance.OnDestinationReached += (projectile, target) =>
        {
            // If there is no on impact effect, don't proceed
            if (ImpactEffect == null) return;

            // Spawn collision particle effect
            var fireEffectInstance = (GameObject)Instantiate(FireEffect, projectile.transform.position, projectile.transform.rotation);

            // Configure debug
            fireEffectInstance.GetComponent<FrostAuraBaseGameObject>().DebugMode = DebugMode;

            // Scale the effect instance accordingly
            fireEffectInstance.transform.localScale = ImpactEffectScale;

            // Inflict damage on enemies
            target.GetComponent<HealthPointsBehaviour>().TakeHealth(Damage);
        };
    }
}